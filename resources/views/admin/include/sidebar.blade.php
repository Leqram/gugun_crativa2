<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="/dashboard" class="app-brand-link">
            <span class="app-brand-text demo menu-text fw-bolder ms-2">Crativa</span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item">
            <a href="/dashboard" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>

        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-layout"></i>
                <div data-i18n="Layouts">Read Update Table</div>
            </a>

            <ul class="menu-sub">
                <li class="menu-item">
                    <a href="/dashboard/homebanner" class="menu-link">
                        <div data-i18n="Without menu">Home Banner</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/dashboard/homequotes" class="menu-link">
                        <div data-i18n="Without navbar">Quotes</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/dashboard/homepersuasive" class="menu-link">
                        <div data-i18n="Container">Persuasive</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/dashboard/homecontacts" class="menu-link">
                        <div data-i18n="Fluid">Contacts</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-layout"></i>
                <div data-i18n="Crud">Crud Table</div>
            </a>

            <ul class="menu-sub">
                <li class="menu-item">
                    <a href="/dashboard/features" class="menu-link">
                        <div data-i18n="Without menu">Features</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="/dashboard/pricing" class="menu-link">
                        <div data-i18n="Without menu">Pricing</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-item">
            <a href="#" class="menu-link" onclick="confirmLogout()">
                <i class="bx bx-power-off me-2"></i>
                <div data-i18n="Analytics">Logout</div>
            </a>
        </li>

        <script>
            function confirmLogout() {
                if (confirm("Are you sure you want to logout?")) {
                    // Jika pengguna menekan OK, lakukan logout
                    window.location.href = "/actionlogout"; // Sesuaikan dengan URL logout Anda
                } else {
                    // Jika pengguna menekan Cancel, tidak melakukan apa-apa
                }
            }
        </script>

    </ul>
</aside>
