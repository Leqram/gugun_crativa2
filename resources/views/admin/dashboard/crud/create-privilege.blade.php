@extends('admin.layouts.master')
@section('content')

    <div class="content-wrapper">
        <!-- Content -->
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Add/ Privilege</span></h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h5 class="mb-0">Privilege new</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('privilege.store') }}" method="POST">
                                @csrf

                                <div class="row mb-3">
                                    <label class="col-sm-2 col-form-label" for="privilege">Privilege</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="privilege" name="privilege" required>
                                    </div>
                                </div>

                                <div class="row justify-content-end">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Create Privilege</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('admin.include.footer')
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
    </div>

@stop
