@extends('admin.layouts.master')
@section('content')

    <div class="content-wrapper">
        <!-- Content -->
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Read update/</span>Homepage Contact</h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h5 class="mb-0">Homepage Contact Now</h5>
                        </div>
                        <div class="card-body">
                            @foreach ($contacts as $contact)
                                <form action="{{ route('update.contacts', $contact->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="row mb-3">
                                        <label class="col-sm-2 form-label" for="basic-icon-default-phone">Phone No</label>
                                        <div class="col-sm-10">
                                            <div class="input-group input-group-merge">
                                                <span id="phone" class="input-group-text"><i
                                                        class="bx bx-phone"></i></span>
                                                <input name="phone" type="text" id="phone"
                                                    class="form-control phone-mask" value="{{ $contact->phone }}"
                                                    aria-label="658 799 8941" aria-describedby="basic-icon-default-phone2">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label" for="basic-icon-default-email">Email</label>
                                        <div class="col-sm-10">
                                            <div class="input-group input-group-merge">
                                                <span class="input-group-text"><i class="bx bx-envelope"></i></span>
                                                <input name="email" type="text" id="basic-icon-default-email"
                                                    class="form-control" value="{{ $contact->email }}" aria-label="john.doe"
                                                    aria-describedby="basic-icon-default-email2">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label class="col-sm-2 col-form-label"
                                            for="basic-icon-default-company">Company</label>
                                        <div class="col-sm-10">
                                            <div class="input-group input-group-merge">
                                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                                        class="bx bx-buildings"></i></span>
                                                <input name="location" type="text" id="basic-icon-default-company"
                                                    class="form-control" value="{{ $contact->location }}"
                                                    aria-label="ACME Inc." aria-describedby="basic-icon-default-company2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-end">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">Send</button>
                                        </div>
                                    </div>
                                </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('admin.include.footer')
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
    </div>

@stop
