<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueConstraintToPricePrivilegeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('price_privilege', function (Blueprint $table) {
            // Menambahkan kunci unik pada price_id dan privilege_id
            $table->unique(['price_id', 'privilege_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('price_privilege', function (Blueprint $table) {
            // Menghapus kunci unik jika rollback migration
            $table->dropUnique(['price_id', 'privilege_id']);
        });
    }
}
