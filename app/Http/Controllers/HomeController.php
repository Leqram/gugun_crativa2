<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Features;
use App\Models\informations;
use Illuminate\Http\Request;
use App\Models\Price;
use App\Models\Privilege;
use App\Models\PricePrivilege;

class HomeController extends Controller
{
    public function getIndexView()
    {
        $prices = Price::all();
        $information = informations::first();
        $contacts = Contact::first();
        $features = Features::all();

        return view('index', compact('prices', 'information', 'contacts', 'features'));
    }
}
